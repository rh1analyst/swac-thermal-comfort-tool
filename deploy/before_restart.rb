node[:deploy].each do |application, deploy|
    deploy_to = deploy[:deploy_to]

    dirs_to_move = ["static", "templates"]
    dirs_to_move.each {|dir| 
        bash "copy #{dir}" do
          code <<-EOL
            cp -a #{release_path}/public/#{dir}/* #{release_path}/public/
          EOL
        end
    }

    bash "remove" do
      code <<-EOL
       # rm -Rf #{release_path}/public/*.py
      EOL
    end

end